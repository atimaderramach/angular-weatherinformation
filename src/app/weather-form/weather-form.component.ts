import {Component} from '@angular/core';
import { Weatherinfo } from '../weatherinfo';
import {Localisation} from "../localisation";
import {WeatherService} from "../weather.service";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-weather-form',
  templateUrl: './weather-form.component.html',
  styleUrls: ['weather-form.component.css']
})
export class WeatherFormComponent {

  weather : Weatherinfo | undefined;
  title = 'WeatherInformation';
  model = new Localisation('','');
  output = new Weatherinfo('','','');
  windSpeed: any;
  temperature: any;
  humidity: any;

  loading : any;
  errorMessage : any;
  submitted = false;


  constructor(private http: HttpClient) {

  }
  weatherService = new WeatherService();


  onSubmit() {  }

  newWeather() {
    this.output = new Weatherinfo('42', '14', '100');

  }


  getWeatherInformation(latitude: string, longitude: string){
    this.loading = true;
    this.errorMessage = "";
    let url : String = this.weatherService.ReturnUrl(latitude, longitude);
    this.http.get<any>(url.toString()).subscribe(data => {
      this.submitted = true;
      this.output.windSpeed = data.windSpeed;
      this.output.temperature = data.temperature;
      this.output.humidity = data.humidity;

    },
      error => {
      this.errorMessage = error;
      this.loading = false;
      alert("No data available for these coordinates, try different ones");

        //throw error;   //You can also throw the error to a global error handler
    })
  }

  export(){
    return this.weatherService.openPDF();
  }

}
