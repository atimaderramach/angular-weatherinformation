import { TestBed } from '@angular/core/testing';
import { WeatherFormComponent } from './weather-form/weather-form.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        WeatherFormComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(WeatherFormComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'WeatherInformation'`, () => {
    const fixture = TestBed.createComponent(WeatherFormComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('WeatherInformation');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(WeatherFormComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('WeatherInformation app is running!');
  });
});
