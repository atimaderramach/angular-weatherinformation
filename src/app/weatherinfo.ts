export class Weatherinfo {

  constructor(
    public temperature: string,
    public windSpeed: string,
    public humidity: string
  ) {  }

}
