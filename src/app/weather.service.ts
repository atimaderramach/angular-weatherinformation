
import {Injectable} from '@angular/core';

import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';



@Injectable()
export class WeatherService {


  private weatherInformationUrl: string | undefined;
  /*
  Method to get the url with the input (longitude and latitude)
   */
  public ReturnUrl(latitude: string, longitude: string) : String{

    let url = new URL('http://localhost:9999/weather/information');
    url.searchParams.append('lat',latitude);
    url.searchParams.append('long',longitude);
    this.weatherInformationUrl = url.toString();
    console.log(this.weatherInformationUrl);
    return this.weatherInformationUrl;


  }

  public openPDF(): void {
    let DATA: any = document.getElementById('pdfTable');
    html2canvas(DATA).then((canvas) => {
      let fileWidth = 208;
      let fileHeight = (canvas.height * fileWidth) / canvas.width;
      const FILEURI = canvas.toDataURL('image/png');
      let PDF = new jsPDF('p', 'mm', 'a4');
      let position = 0;
      PDF.addImage(FILEURI, 'PNG', 0, position, fileWidth, fileHeight);
      PDF.save('weatherInformation.pdf');
    });
  }

}
